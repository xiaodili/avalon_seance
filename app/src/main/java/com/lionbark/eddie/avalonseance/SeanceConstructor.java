package com.lionbark.eddie.avalonseance;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/**
 * Created by Eddie on 7/4/2015.
 */
public class SeanceConstructor {


    public static SeancePart[] Create(Context context, int numPlayers,
                                      boolean percival,
                                      boolean mordred,
                                      boolean morgana,
                                      boolean oberon,
                                      long defaultPause) {

        HashMap<Integer, Integer> players2Baddies = new HashMap<>();
        players2Baddies.put(5, 2);
        players2Baddies.put(6, 2);
        players2Baddies.put(7, 3);
        players2Baddies.put(8, 3);
        players2Baddies.put(9, 3);
        players2Baddies.put(10, 4);

        List<SeancePart> seanceParts = new ArrayList<>();

        seanceParts.add(new SeancePart(context.getString(R.string.seance_start)));
        seanceParts.add(new SeancePart(defaultPause));

        if (oberon) {
            seanceParts.add(new SeancePart(context.getString(R.string.seance_minions_open_with_oberon)));
        } else {
            seanceParts.add(new SeancePart(context.getString(R.string.seance_minions_open)));
        }
        //needs to be longer!
        seanceParts.add(new SeancePart(defaultPause * players2Baddies.get(numPlayers)));

        seanceParts.add(new SeancePart(context.getString(R.string.seance_minions_close)));
        seanceParts.add(new SeancePart(defaultPause));

        seanceParts.add(new SeancePart(context.getString(R.string.seance_confirm_closed)));
        seanceParts.add(new SeancePart(defaultPause));

        if (mordred) {
            seanceParts.add(new SeancePart(context.getString(R.string.seance_minions_thumb_up_with_mordred)));
        } else {
            seanceParts.add(new SeancePart(context.getString(R.string.seance_minions_thumb_up)));
        }
        seanceParts.add(new SeancePart(defaultPause));

        seanceParts.add(new SeancePart(context.getString(R.string.seance_merlin_open)));
        seanceParts.add(new SeancePart(defaultPause * players2Baddies.get(numPlayers)));

        seanceParts.add(new SeancePart(context.getString(R.string.seance_minions_thumb_down)));
        seanceParts.add(new SeancePart(defaultPause));

        seanceParts.add(new SeancePart(context.getString(R.string.seance_merlin_close)));
        seanceParts.add(new SeancePart(defaultPause));

        seanceParts.add(new SeancePart(context.getString(R.string.seance_confirm_closed)));
        seanceParts.add(new SeancePart(defaultPause));

        if (percival) {
            if (morgana) {
                seanceParts.add(new SeancePart(context.getString(R.string.seance_merlin_thumb_up_with_morgana)));
                seanceParts.add(new SeancePart(defaultPause));

                seanceParts.add(new SeancePart(context.getString(R.string.seance_percival_open_with_morgana)));
                seanceParts.add(new SeancePart(defaultPause * 2));

                seanceParts.add(new SeancePart(context.getString(R.string.seance_merlin_thumb_down_with_morgana)));
                seanceParts.add(new SeancePart(defaultPause));
            } else {
                seanceParts.add(new SeancePart(context.getString(R.string.seance_merlin_thumb_up)));
                seanceParts.add(new SeancePart(defaultPause));

                seanceParts.add(new SeancePart(context.getString(R.string.seance_percival_open)));
                seanceParts.add(new SeancePart(defaultPause));

                seanceParts.add(new SeancePart(context.getString(R.string.seance_merlin_thumb_down)));
                seanceParts.add(new SeancePart(defaultPause));

            }
            seanceParts.add(new SeancePart(context.getString(R.string.seance_percival_close)));
            seanceParts.add(new SeancePart(defaultPause));

            seanceParts.add(new SeancePart(context.getString(R.string.seance_confirm_closed)));
            seanceParts.add(new SeancePart(defaultPause));
        }

        seanceParts.add(new SeancePart(context.getString(R.string.seance_finish)));
        seanceParts.add(new SeancePart(defaultPause));

        System.out.println(seanceParts.size());
        return seanceParts.toArray(new SeancePart[seanceParts.size()]);
    }
}
