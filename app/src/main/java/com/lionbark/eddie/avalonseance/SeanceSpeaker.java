package com.lionbark.eddie.avalonseance;
import android.speech.tts.TextToSpeech;

/**
 * Created by Eddie on 7/4/2015.
 */
public class SeanceSpeaker {
    private TextToSpeech tts;

    public SeanceSpeaker(TextToSpeech tts) {
        this.tts = tts;
    }

    public void speak(SeancePart[] seanceParts) {
        tts.speak(seanceParts[0].content, TextToSpeech.QUEUE_FLUSH, null);
        for (int i = 1; i < seanceParts.length; i++) {
            if (seanceParts[i].isSpeech) {
                tts.speak(seanceParts[i].content, TextToSpeech.QUEUE_ADD, null);
            } else {
                tts.playSilence(seanceParts[i].duration, TextToSpeech.QUEUE_ADD, null);
            }
        }
    }
}