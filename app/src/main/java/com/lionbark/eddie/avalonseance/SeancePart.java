package com.lionbark.eddie.avalonseance;

/**
 * Created by Eddie on 7/4/2015.
 */
public class SeancePart {
    public String content;
    public boolean isSpeech;
    public long duration;

    public SeancePart(String content) {
        this.isSpeech = true;
        this.content = content;
    }

    public SeancePart(long duration) {
        this.isSpeech = false;
        this.duration = duration;
    }
}