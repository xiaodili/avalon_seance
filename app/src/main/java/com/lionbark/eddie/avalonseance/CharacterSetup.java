package com.lionbark.eddie.avalonseance;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.speech.tts.TextToSpeech;
import android.widget.CompoundButton;
import android.widget.NumberPicker;

import java.util.Locale;

public class CharacterSetup extends ActionBarActivity {

    SeanceSpeaker seanceSpeaker;
    TextToSpeech tts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_setup);
        final NumberPicker numPlayers = (NumberPicker) findViewById(R.id.num_players);
        final Button seance = (Button) findViewById(R.id.seance);
        final CheckBox morgana = (CheckBox) findViewById(R.id.morgana);
        final CheckBox percival = (CheckBox) findViewById(R.id.percival);
        final Context ctx = getApplicationContext();

        tts = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.UK);
                }
            }
        });

        seanceSpeaker = new SeanceSpeaker(tts);

        //adding constraints on number
        numPlayers.setMaxValue(10);
        numPlayers.setMinValue(5);

        //making sure percival is included if morgana is selected
        morgana.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    percival.setChecked(true);
                }
            }
        });

        //if percival is excluded, exclude morgana
        percival.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    morgana.setChecked(false);
                }
            }
        });

        //adding button to seance
        seance.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boolean percival = ((CheckBox) findViewById(R.id.percival)).isChecked();
                boolean mordred = ((CheckBox) findViewById(R.id.mordred)).isChecked();
                boolean morgana = ((CheckBox) findViewById(R.id.morgana)).isChecked();
                boolean oberon = ((CheckBox) findViewById(R.id.oberon)).isChecked();
                int numPlayers = ((NumberPicker) findViewById(R.id.num_players)).getValue();
                SeancePart[] seanceParts = SeanceConstructor.Create(ctx, numPlayers, percival, mordred, morgana, oberon, 2000);
                seanceSpeaker.speak(seanceParts);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_character_setup, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}